$(document).ready(function() {
  var getUrlParameter, information, parseConditions, parseJson, refresh, resort_id, resort_name, sectors, setDate, trailStatus, update;
  sectors = {};
  trailStatus = {
    'closing': 'C',
    'opening': 'O',
    'race': 'R',
    'grooming': 'G',
    'noon_groomed': 'N',
    'snowmaking': 'S',
    'expected_opening': 'E'
  };
  information = {
    trail_open: 0,
    trail_open_acres: 0,
    trail_total: 0,
    lift_open: 0,
    lift_total: 0,
    types: {
      total: 0,
      green: 0,
      blue: 0,
      black: 0,
      double_black: 0,
      extreme_double_black: 0
    }
  };
  setDate = function() {
    var date, date_options;
    date = new Date();
    date_options = {
      weekday: "long",
      year: "numeric",
      month: "long",
      day: "numeric",
      hour: "2-digit",
      minute: "2-digit"
    };
    date = date.toLocaleTimeString("en-us", date_options);
    return $('#date').html(date);
  };
  parseJson = function(resort_id) {
    return $.ajax({
      dataType: 'json',
      url: 'https://s3.us-east-2.amazonaws.com/tech4snow/feeds/' + resort_id + '/status.json',
      success: function(data) {
        var html, html_, i, lift_status, lifts, sectorKey, trailing_lis;
        lifts = [];
        html = '';
        trailing_lis = '';
        i = 0;
        while (i < 20) {
          i++;
          trailing_lis += '<li class="trailing"></li>';
        }
        data.forEach(function(obj) {
          if (obj.type === 'lift') {
            obj.status.forEach(function(status) {
              if (status.status_name === 'opening' && status.status_value === 'true') {
                obj.status = 'opening';
              } else {
                obj.status = 'closing';
              }
              return obj.short_status = trailStatus[obj.status];
            });
            return lifts[obj.id] = obj;
          }
        });
        lifts[99999] = {
          status: '',
          short_status: '',
          properties: {
            name: 'Cross Country Trails'
          }
        };
        sectors[99998] = {
          name: 'Terrain Parks',
          status: '',
          short_status: '',
          trails: []
        };
        data.forEach(function(obj) {
          var sector_id;
          if ((obj.type === 'trail' && obj.lift_id) || obj.type === 'cross country trail') {
            sector_id = obj.lift_id;
            if (obj.type === 'cross country trail') {
              sector_id = 99999;
            }
            if (typeof sectors[sector_id] === 'undefined') {
              sectors[sector_id] = {
                name: lifts[sector_id].properties.name,
                status: lifts[sector_id].status,
                short_status: lifts[sector_id].short_status,
                trails: []
              };
              if (lifts[sector_id].status === 'opening') {
                information.lift_open++;
              }
              information.lift_total++;
              sectors[sector_id].trails.push(obj);
              information.trail_total++;
            }
          }
          if (obj.type === 'area' && obj.properties.subtype === 'snowpark') {
            obj.properties.subtype = '';
            sectors[99998].trails.push(obj);
          }
        });
        for (sectorKey in sectors) {
          if (sectors.hasOwnProperty(sectorKey)) {
            html_ = '';
            sectors[sectorKey].trails.forEach(function(trail) {
              var html__, trailStatusKey;
              html__ = 'C';
              for (trailStatusKey in trailStatus) {
                if (trailStatus.hasOwnProperty(trailStatusKey)) {
                  trail.status.forEach(function(status) {
                    var area;
                    if (status.status_name === trailStatusKey && status.status_value === 'true') {
                      html__ = trailStatus[trailStatusKey];
                      if (trailStatus[trailStatusKey] === 'O') {
                        area = parseFloat(trail.area);
                        information.trail_open++;
                        information.trail_open_acres = information.trail_open_acres + area;
                        if (trail.properties.subtype === 'easier') {
                          information.types.green += area;
                        }
                        if (trail.properties.subtype === 'more_difficult') {
                          information.types.blue += area;
                        }
                        if (trail.properties.subtype === 'most_difficult') {
                          information.types.black += area;
                        }
                        if (trail.properties.subtype === 'extremely_difficult') {
                          information.types.double_black += area;
                        }
                        return information.types.total += area;
                      }
                    }
                  });
                }
              }
              if (trail.properties.subtype !== '') {
                return html_ += '<li><h3>' + html__ + '<img src="images/track_trail_' + trail.properties.subtype + '.png" />' + trail.properties.name + '</h3></li>';
              } else {
                return html_ += '<li><h3>' + html__ + ' ' + trail.properties.name + '</h3></li>';
              }
            });
            if (sectors[sectorKey].short_status !== '') {
              lift_status = '[' + sectors[sectorKey].short_status + ']';
            } else {
              lift_status = '';
            }
            html += '<li><h2>' + lift_status + ' ' + sectors[sectorKey].name + '</h2><ul>' + html_ + '</ul></li>';
          }
        }
        $('#sectors > ul').html(html + trailing_lis);
        $('#trail_open').html(information.trail_open);
        $('#trail_total').html(information.trail_total);
        $('#trail_open_acres').html(information.trail_open_acres > 0 ? information.trail_open_acres : 0);
        $('#lift_open').html(information.lift_open);
        $('#lift_total').html(information.lift_total);
        $('#type_green').html(information.types.green > 0 ? Math.round((information.types.green / information.types.total) * 100) + '%' : 0);
        $('#type_blue').html(information.types.blue > 0 ? Math.round((information.types.blue / information.types.total) * 100) + '%' : 0);
        $('#type_black').html(information.types.black > 0 ? Math.round((information.types.black / information.types.total) * 100) + '%' : 0);
        $('#type_double_black').html(information.types.double_black > 0 ? Math.round((information.types.double_black / information.types.total) * 100) + '%' : 0);
      }
    });
  };
  parseConditions = function(resort_id) {
    return $.ajax({
      dataType: 'json',
      url: 'https://s3.us-east-2.amazonaws.com/tech4snow/feeds/' + resort_id + '/conditions.json',
      success: function(data) {
        var report;
        report = $.grep(data.snowReport, function(v) {
          return v.measurement_location_name === 'Primary';
        })[0];
        report.items.forEach(function(item) {
          if (item.duration === 'month') {
            $('#snow_total_month').html(item.amount + '"');
          }
          if (item.duration === 'total') {
            $('#snow_total').html(item.amount + '"');
          }
          if (item.duration === '24 Hours') {
            $('#snow_24h').html(item.amount + '"');
          }
          if (item.duration === '48 Hours') {
            $('#snow_48h').html(item.amount + '"');
          }
          if (item.duration === '72 Hours') {
            $('#snow_72h').html(item.amount + '"');
          }
          if (item.duration === 'base-depth') {
            $('#snow_mid').html(item.amount + '"');
          }
          if (item.duration === 'base-depth') {
            return $('#snow_upper').html(item.amount + '"');
          }
        });
      }
    });
  };
  getUrlParameter = function(sParam) {
    var i, sPageURL, sParameterName, sURLVariables;
    sPageURL = decodeURIComponent(window.location.search.substring(1));
    sURLVariables = sPageURL.split('&');
    sParameterName = void 0;
    i = void 0;
    i = 0;
    while (i < sURLVariables.length) {
      sParameterName = sURLVariables[i].split('=');
      if (sParameterName[0] === sParam) {
        if (sParameterName[1] === void 0) {
          return true;
        } else {
          return sParameterName[1];
        }
      }
      i++;
    }
  };
  update = function(resort_id, status) {
    var form;
    form = new FormData();
    form.append("resort", resort_id);
    form.append("generate", status);
    return $.ajax({
      type: "POST",
      async: false,
      crossDomain: true,
      contentType: false,
      processData: false,
      url: 'http://api-tech4snow.us-east-1.elasticbeanstalk.com/api/generate',
      data: form,
      headers: {
        authorization: 'Bearer CEA4fS7npFEvhR7Iy5VsolZodAgh3ZzlCynkvyS945NVzSdUh0L1Jt99lbcW'
      },
      success: function(data) {
        console.log(data);
      }
    });
  };
  refresh = getUrlParameter('refresh');
  resort_id = getUrlParameter('resort_id');
  resort_name = getUrlParameter('resort_name').replace(new RegExp('\\+', 'g'), ' ');
  $('#resort_name').html(decodeURIComponent(resort_name));
  if (refresh === 1234) {
    update(resort_id, 'status');
  }
  setDate();
  parseJson(resort_id);
  parseConditions(resort_id);
});