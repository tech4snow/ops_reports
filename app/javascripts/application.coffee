$(document).ready ->
	sectors = {}

	trailStatus =
		'closing': 'C'
		'opening': 'O'
		'race': 'R'
		'grooming': 'G'
		'noon_groomed': 'N'
		'snowmaking': 'S'
		'expected_opening': 'E'

	information = 
		trail_open: 0
		trail_open_acres: 0
		trail_total: 0
		lift_open: 0
		lift_total: 0
		types:
			green:
				open: 0
				total: 0
			blue:
				open: 0
				total: 0
			black:
				open: 0
				total: 0
			double_black:
				open: 0
				total: 0

	setDate = (paramDate = '') ->
		if paramDate == '' then date = new Date() else date = new Date(paramDate)
		date_options =
		    weekday: "long", year: "numeric", month: "long",  
		    day: "numeric", hour: "2-digit", minute: "2-digit"  

		date = date.toLocaleTimeString("en-us", date_options)
		$('#date').html date

	parseJson = (resort_id, schedule, content = '') ->
		if schedule then url = 'https://s3.us-east-2.amazonaws.com/tech4snow/feeds/'+resort_id+'/status_scheduled.json' else url = 'https://s3.us-east-2.amazonaws.com/tech4snow/feeds/'+resort_id+'/status.json'
		$.ajax
			dataType: 'json',
			url : url
			success: (data) ->
				dataToHtml resort_id, data
				putActivities data
			error: (data) ->
				console.log 'using storage status.json'
				data = JSON.parse(content.status)
				dataToHtml 0, data, content.conditions

	putActivities = (data) ->
		activities = ''

		data.forEach (obj) ->
			if obj.type == 'activity'
				obj.status.forEach (status) ->
					if status.status_name == 'opening' and status.status_value == 'true' then obj.status = 'opening' else obj.status = 'closing'
					obj.short_status = trailStatus[obj.status]
				activities += '<dd>['+obj.short_status+'] '+obj.properties.name+'</dd>'

		$('#winter_activities > dl').html $('#winter_activities > dl').html() + activities

	dataToHtml = (resort_id, data, conditions = '') ->
		lifts = []
		html = ''		
		###trailing_lis = ''

		i = 0
		while i < 30
			i++
			trailing_lis += '<li class="trailing"></li>'###

		data.forEach (obj) ->
			if obj.type == 'lift'
				obj.status.forEach (status) ->
					if status.status_name == 'opening' and status.status_value == 'true' then obj.status = 'opening' else obj.status = 'closing'
					obj.short_status = trailStatus[obj.status]
				obj.filled = false
				lifts[obj.id] = obj

		# for cross country
		lifts[99999] = 
			status: ''
			short_status: ''
			properties:
				name: 'Cross Country Trails'

		###sectors[99998] = 
			name: 'Terrain Parks'
			status: ''
			short_status: ''
			trails: []###

		data.forEach (obj) ->
			if (obj.type == 'trail' and obj.lift_id) or obj.type == 'cross country trail'
				if obj.lift_id != '' then sector_id = obj.lift_id  else sector_id = obj.id

				if obj.type == 'cross country trail' then sector_id = 99999

				if typeof sectors[sector_id] == 'undefined'
					lifts[sector_id].filled = true
					sectors[sector_id] =
						name: lifts[sector_id].properties.name
						status: lifts[sector_id].status
						short_status: lifts[sector_id].short_status
						trails: []
					if sector_id != 99999
						if lifts[sector_id].status == 'opening' then information.lift_open++
						information.lift_total++

				sectors[sector_id].trails.push obj
				information.trail_total++

			###if obj.type == 'area' and obj.properties.subtype == 'snowpark'
				obj.properties.subtype = ''
				sectors[99998].trails.push obj###

			return

		lifts.forEach (obj) ->
			if !obj.filled
				aux =
					name: obj.properties.name
					status: obj.status
					short_status: obj.short_status
					trails: []
				sectors[obj.id] = aux

				information.lift_total++
				if obj.status == 'opening' then information.lift_open++

		for sectorKey of sectors
			if sectors.hasOwnProperty(sectorKey)
				# trails
				html_ = ''
				sectors[sectorKey].trails.forEach (trail) ->
					html__ = 'C'

					area = parseFloat(trail.area)

					if trail.properties.subtype == 'easier' then information.types.green.total += area
					if trail.properties.subtype == 'more_difficult' then information.types.blue.total += area
					if trail.properties.subtype == 'most_difficult' then information.types.black.total += area
					if trail.properties.subtype == 'extremely_difficult' then information.types.double_black.total += area

					for trailStatusKey of trailStatus
						if trailStatus.hasOwnProperty(trailStatusKey)
							trail.status.forEach (status) ->
								if status.status_name == trailStatusKey and status.status_value == 'true'
									if html__ == 'O' and status.status_name == 'grooming'
										html__ = 'OG'
									else
										html__ = trailStatus[trailStatusKey]
					if html__ == 'O' || html__ == 'OG' || html__ == 'E' || html__ == 'G'
						information.trail_open++				
						information.trail_open_acres += area

						if trail.properties.subtype == 'easier' then information.types.green.open += area
						if trail.properties.subtype == 'more_difficult' then information.types.blue.open += area
						if trail.properties.subtype == 'most_difficult' then information.types.black.open += area
						if trail.properties.subtype == 'extremely_difficult' then information.types.double_black.open += area

					if trail.properties.subtype != ''
						html_ += '<li><h3>'+html__+'<img src="images/track_trail_'+trail.properties.subtype+'.png" />'+trail.properties.name+'</h3></li>'
					else
						html_ += '<li><h3>'+html__+' '+trail.properties.name+'</h3></li>'

				if sectors[sectorKey].short_status != '' then lift_status = '['+sectors[sectorKey].short_status+']' else lift_status = ''
				html += '<li><h2>'+lift_status+' '+sectors[sectorKey].name+'</h2><ul>'+html_+'</ul></li>'

		# $('#sectors > ul').html html+trailing_lis
		$('#sectors > ul').html html



		$('#type_green').html if information.types.green.open > 0 then Math.round((information.types.green.open / information.types.green.total)*100) + '%' else 0 + '%'
		$('#type_blue').html if information.types.blue.open > 0 then Math.round((information.types.blue.open / information.types.blue.total)*100) + '%' else 0 + '%'
		$('#type_black').html if information.types.black.open > 0 then Math.round((information.types.black.open / information.types.black.total)*100) + '%' else 0 + '%'
		$('#type_double_black').html if information.types.double_black.open > 0 then Math.round((information.types.double_black.open / information.types.double_black.total)*100) + '%' else 0 + '%'

		parseConditions resort_id, conditions
		return

	parseConditions = (resort_id, content = '') ->
		$.ajax
			dataType: 'json'
			url: 'https://s3.us-east-2.amazonaws.com/tech4snow/feeds/'+resort_id+'/conditions.json'
			success: (data) ->
				conditionsToHtml data
			error: (data) ->
				console.log 'using storage conditions.json'
				data = JSON.parse(content)
				conditionsToHtml data

	conditionsToHtml = (data) ->
		report = $.grep(data.snowReport, (v) ->
			v.measurement_location_name == 'Primary'
		)[0]
		trailReport = data.trailReport
		liftReport = data.liftReport

		$('#trail_open').html trailReport.open
		$('#trail_total').html trailReport.total
		$('#trail_open_acres').html if trailReport.area.open > 0 then trailReport.area.open.toFixed(2) else 0
		$('#lift_open').html liftReport.open
		$('#lift_total').html liftReport.total
		report.items.forEach (item) ->
			if item.duration == 'This Month' then $('#snow_total_month').html item.amount + '"'
			if item.duration == 'total' then $('#snow_total').html item.amount + '"'
			if item.duration == '24 Hours' then $('#snow_24h').html item.amount + '"'
			if item.duration == '48 Hours' then $('#snow_48h').html item.amount + '"'
			if item.duration == '72 Hours' then $('#snow_72h').html item.amount + '"'
			if item.duration == 'base-depth' then $('#snow_mid').html item.amount + '"'
			if item.duration == 'upper-base-depth' then $('#snow_upper').html item.amount + '"' #need another source of data

		list = parks = ''
		data.parkUpdate.forEach (park) ->
			parks += '<li><h3>'+park.park_name+'</h3></li>'
		list = '<li><h2>Terrain Parks</h2><ul>'+parks+'</ul></li>'
		$('#sectors > ul').html $('#sectors > ul').html() + list

		return

	getUrlParameter = (sParam) ->
		sPageURL = decodeURIComponent(window.location.search.substring(1))
		sURLVariables = sPageURL.split('&')
		sParameterName = undefined
		i = undefined
		i = 0
		while i < sURLVariables.length
			sParameterName = sURLVariables[i].split('=')
			if sParameterName[0] == sParam
		  		return if sParameterName[1] == undefined then true else sParameterName[1]
			i++
		return

	update = (resort_id, status) ->
		form = new FormData()
		form.append("resort", resort_id)
		form.append("generate", status)

		$.ajax
			type: "POST"
			async: false
			crossDomain: true
			contentType: false
			processData: false
			url: 'http://api-tech4snow.us-east-1.elasticbeanstalk.com/api/generate'
			data: form
			headers:
				authorization: 'Bearer CEA4fS7npFEvhR7Iy5VsolZodAgh3ZzlCynkvyS945NVzSdUh0L1Jt99lbcW'
			success: (data) ->
				console.log data

				return

	queryByFilename = (resort_id, filename) ->
		returnData = {}

		$.ajax
			dataType: 'json'
			url: 'http://api-tech4snow.us-east-1.elasticbeanstalk.com/api/ops_report?resort='+resort_id+'&filename='+filename+''
			headers:
				authorization: 'Bearer CEA4fS7npFEvhR7Iy5VsolZodAgh3ZzlCynkvyS945NVzSdUh0L1Jt99lbcW'
			success: (data) ->
				data = data.data.data[0]
				setDate(data.date)
				parseJson 0, data

	filename = getUrlParameter('filename')
	refresh = getUrlParameter('refresh')
	schedule = getUrlParameter('schedule')
	resort_id = getUrlParameter('resort_id')
	resort_name = getUrlParameter('resort_name').replace(new RegExp('\\+', 'g'), ' ')
	document.title = resort_name + ' - Daily trail and information sheet'
	$('#resort_name').html decodeURIComponent(resort_name)

	if filename
		globalData = queryByFilename resort_id, filename
	else
		if parseInt(refresh) == 1234
			update resort_id, 'status'
		setDate()
		parseJson resort_id , schedule

	$('#generatePDF').click ->
		$('#generatePDF').hide()
		ExportPdf()

	return


